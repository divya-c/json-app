import React from "react";
import {JsonEditor as Editor} from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';

const PopUp = ({
  popUpData,
  closePopUp
}) => {

  const updateData = () => {
    console.log(popUpData);
    closePopUp();
  }

  const handleChange = (updatedJSON) => {
    console.log("updatedJSON", updatedJSON);
     // make a PUT request with the updatedJSON
  }
  return (
    <div className="popup-container">
       <div className="popup-content">
          <Editor
              value={popUpData}
              onChange={handleChange}
          />
        </div> 
        <div 
          onClick={updateData}
          className="close-btn" 
        />
    </div>
  );
}

export default PopUp;