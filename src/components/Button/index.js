import React, { useState, useEffect } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { updateData } from "../../store/data/actions";

const Button = ({ dispatchData }) => {
  const [fetched, setFetched] = useState(false);

  const fetchData = async () => {
    try {
      const config = {
        headers: {
          "secret-key":
            "$2b$10$aLR/hcdPF1V6yUbqIHs93OFqeHPwKYWQLaPijFjTltpy8U150GNly"
        }
      };
      const res = await axios.get("");
      console.log("Res", res.data.keys);
      const { keys } = res.data;
      let filteredData = {};

      for (let key in keys) {
        const item = keys[key][0];
        const endTime = item.endUnixTimestampInMillisec;
        const startTime = item.startUnixTimestampInMillisec;
        if (Date.now() < endTime && Date.now() > startTime) {
          filteredData[key] = item;
        }
      }

      if (filteredData) {
        setFetched(true);
      }
      dispatchData(filteredData);
    } catch (err) {
      console.log("err", err);
    }
  };

  return (
    <button className="button" onClick={fetchData} disabled={fetched}>
      FETCH DATA
    </button>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatchData: data => {
    dispatch(updateData(data));
  }
});

export default connect(null, mapDispatchToProps)(Button);
