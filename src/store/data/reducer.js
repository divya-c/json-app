import {UPDATE_DATA} from "../types";

const initialState = {};

export const updateReducer = (state = initialState, action) => {
  if (action.type === UPDATE_DATA) {
    const updatedState = {...state, ...action.value};
    return updatedState;
  }
  return state;
}


