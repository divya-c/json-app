import {updateReducer as updatedData} from "./data/reducer";
import {combineReducers} from "redux";

const data = combineReducers({
  updatedData
})

export default data;
